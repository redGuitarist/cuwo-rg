# Copyright (c) Mathias Kaerlev 2013-2017.
#
# This file is part of cuwo.
#
# cuwo is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# cuwo is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with cuwo.  If not, see <http://www.gnu.org/licenses/>.

"""
Default set of commands bundled with cuwo
"""

from cuwo.script import ServerScript, command, admin, alias
from cuwo.common import get_chunk
from cuwo import constants
from cuwo import static
from cuwo.vector import vec3, qvec3
import platform
from scripts.commands import teleport


class WarpServer(ServerScript):
    def on_load(self):
        self.warps = self.server.load_data('warps', {})

    def save(self):
        self.server.save_data('warps', self.warps)

    def setwarp(self, name, pos):
        self.warps[name] = list(pos) # explicit copy
        self.save()

    def delwarp(self, name):
        self.warps.pop(name, None)
        self.save()

    def getwarp(self, name):
        if name in self.warps:
            return self.warps[name]
        else:
            return None

    def getwarps(self):
        return self.warps.keys()

def get_class():
    return WarpServer

def create_teleport_packet(pos, chunk_pos, user_id):
    packet = static.StaticEntityPacket()
    header = static.StaticEntityHeader()
    packet.header = header
    packet.chunk_x = chunk_pos[0]
    packet.chunk_y = chunk_pos[1]
    packet.entity_id = 0
    header.set_type('Bench')
    header.size = vec3(0, 0, 0)
    header.closed = True
    header.orientation = static.ORIENT_SOUTH
    header.pos = pos
    header.time_offset = 0
    header.something8 = 0
    header.user_id = user_id
    return packet


@command
@admin
def setwarp(script, name):
    """Remeber a warp"""
    entity = script.connection.entity
    pos = entity.pos
    script.parent.setwarp(name, pos)
    return f'Created warp {name}'

@command
@admin
def delwarp(script, name):
    script.parent.delwarp(name)
    return f'Removed warp {name}'

@command
def warps(script):
    response = 'Available warps:\n'
    for warp in script.parent.getwarps():
        response += str(warp) + ' '

    return response

@command
@alias('w')
def warp(script, name):
    """Teleport to a warp."""
    entity = script.connection.entity

    pos = script.parent.getwarp(name)
    if pos == None:
        return 'No such warp'

    update_packet = script.server.update_packet
    chunk = script.connection.chunk

    packet = create_teleport_packet(pos, chunk.pos, entity.entity_id)
    update_packet.static_entities.append(packet)

    def send_reset_packet():
        if chunk.static_entities:
            chunk.static_entities[0].update()
        else:
            packet = create_teleport_packet(pos, chunk.pos, 0)
            update_packet.static_entities.append(packet)

    script.loop.call_later(0.1, send_reset_packet)

